package main

import (
"fmt"
"github.com/spf13/cobra"
)

var devoir = &cobra.Command{
	Use:   "devoir",
	Short: "Affiche commande",
	Long: "Affiche les commande de devoir",
	Run: devoir,
}

func devoir(cmd *cobra.Command, args []string) {
	fmt.Println("Les commandes disponibles sont un et deux")
}


var devoirUn = &cobra.Command{
	Use:   "un",
	Short: "Affiche Un",
	Long: "Permet d'afficher le nombre un",
	Run: devoir1,
}

func devoir1(cmd *cobra.Command, args []string) {
	fmt.Println("un")
}

var devoirDeux = &cobra.Command{
	Use:   "deux",
	Short: "Affiche Deux",
	Long: "Permet d'afficher le nombre deux",
	Run: devoir2,
}

func devoir2(cmd *cobra.Command, args []string) {
	fmt.Println("deux")
}

func Execute() {
  devoir.Execute()
}


func init() {
	devoir.AddCommand(devoirUn)
	devoir.AddCommand(devoirDeux)
}

 func main() {
  Execute() 
}
